const { exec } = require("child_process");
const loading = require("loading-cli");
let data = [
  {
    username: "",
    password: "",
    database: "",
    host: "",
    port: 5432,
    dialect: "postgres",
    logging: false,
    schema: "public",
  },
];

async function init() {
  for (let { username, password, database, host, port, schema } of data) {
    let load = loading(`dumping ${host}`).start();
    let fileName = `${username}-${database}-${schema}-${Date.now()}`;
    let cmd = `PGPASSWORD=${password} pg_dump -h ${host} -U ${username} -f ${fileName}.sql -d ${database} -F p -p ${port} -N ${schema}`;
    await exec(cmd, (error, stdout, stderr) => {
      if (error) console.log("error >> " + error.message);

      if (stdout) {
        console.log(stdout);
      }

      if (stderr) {
        console.log(stderr);
      }
      load.succeed(`${host} dumped`);
      load.stop();
    });
  }
}

init();
